# What is this?

I use this to keep .torrent files I have.

Only used for legal torrenting, of course.
Mainly, used for sharing files via BitTorrent protocol when they offer really bad hosting, for the purpose of keeping them archived, or even just for faster downloads.

If a file has a static link, it is included in the file as well.

Use of [qBittorrent](https://qbittorrent.org) is encouraged, and if possible, using lt20. Torrents here try to either be hybrid, or v2. Transmission broke with these IIRC.

## Note

I am not currently keeping files as alive as they could, I did do this before in 2023 when I had a PC I had turned on for hosting anyway, but that's not currently the case. I try to when I can.